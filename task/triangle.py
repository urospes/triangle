def is_triangle(a, b, c):
    '''
    please add your solution here or call your solution implemented in different function from here  
    then change return value from 'False' to value that will be returned by your solution
    '''
    # if any of the lengths is < 0, there is no solution
    if a < 0 or b < 0 or c < 0:
        return False
    
    #if the lenghs are valid, we check if sum of every to is greater then the 3rd
    result = a + b > c and a + c > b and b + c > a

    return result
